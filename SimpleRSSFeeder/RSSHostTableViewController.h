//
//  RSSHostTableViewController.h
//  SimpleRSSFeeder
//
//  Created by Konstantin Beltikov on 19.12.15.
//  Copyright © 2015 none. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSSXMLParser.h"
#import <CoreData/CoreData.h>
#import "RSSItemsTableViewController.h"

@interface RSSHostTableViewController : UITableViewController<NSFetchedResultsControllerDelegate> {
    NSArray *testTitlesArray;
    NSArray *testDetailsArray;
}

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

- (void) updateAllItems;
@end
