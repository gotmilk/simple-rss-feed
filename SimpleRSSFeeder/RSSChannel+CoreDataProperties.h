//
//  RSSChannel+CoreDataProperties.h
//  SimpleRSSFeeder
//
//  Created by Konstantin Beltikov on 21.12.15.
//  Copyright © 2015 none. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RSSChannel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RSSChannel (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) NSString *desc;
@property (nullable, nonatomic, retain) NSString *link;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSSet<RSSItem *> *items;

@end

@interface RSSChannel (CoreDataGeneratedAccessors)

- (void)addItemsObject:(RSSItem *)value;
- (void)removeItemsObject:(RSSItem *)value;
- (void)addItems:(NSSet<RSSItem *> *)values;
- (void)removeItems:(NSSet<RSSItem *> *)values;

@end

NS_ASSUME_NONNULL_END
