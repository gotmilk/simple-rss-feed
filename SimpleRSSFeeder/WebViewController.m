//
//  WebViewController.m
//  SimpleRSSFeeder
//
//  Created by Konstantin Beltikov on 22.12.15.
//  Copyright © 2015 none. All rights reserved.
//

#import "WebViewController.h"

@implementation WebViewController

// Under @implementation
@synthesize webView = _webView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _webView.delegate = self;
    
//    self.itemURL = [self.itemURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    NSLog(@"%@", self.itemURL);
//    NSURL *url = [NSURL URLWithString:normalString];
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [_webView setScalesPageToFit:YES];
    
}

// Add new methods
- (void)viewWillAppear:(BOOL)animated {

    NSURL *url = [NSURL URLWithString:self.itemURL];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];

}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"Error : %@",error);
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
