//
//  RSSXMLParser.m
//  SimpleRSSFeeder
//
//  Created by Konstantin Beltikov on 19.12.15.
//  Copyright © 2015 none. All rights reserved.
//

#import "RSSXMLParser.h"

@implementation RSSXMLParser

@synthesize feeds, item, title, link, element, pubDate, desc, rssLink, rssDesc, rssTitle;

/*
 * Delegate NSXMLParser init methods
 *
 */
- (instancetype)init
{
    if (self = [super init]) {
        parser = [[NSXMLParser alloc] init];
        [self configParser];
    }
    
    return self;
}

- (instancetype)initWithContentsOfURL:(NSString *)url
{
    if (self = [super init]) {
        rssLink = url;
        
        // Spin
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        parser = [[NSXMLParser alloc] initWithContentsOfURL:[NSURL URLWithString:url]];
        [self configParser];
    }
    
    return self;
}

- (instancetype)initWithData:(NSData *)data
{
    if (self = [super init]) {
        parser = [[NSXMLParser alloc] initWithData:data];
        [self configParser];
    }
    
    return self;
}

- (instancetype)initWithStream:(NSInputStream *)stream NS_AVAILABLE(10_7, 5_0)
{
    if (self = [super init]) {
        parser = [[NSXMLParser alloc] initWithStream:stream];
        [self configParser];
    }
    
    return self;
}

- (void)configParser
{
    if (parser) {
        parser.delegate = self;
        [parser setShouldResolveExternalEntities:NO];
    }
}

/*
 *  Parse delegate
 *
 */
- (void)parse
{
    if (parser) {
        [parser parse];
    }
}


/*
 * Parser methods (as delegate for parser)
 *
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict;
{
    element = elementName;
    
    if ([elementName isEqualToString:@"rss"]) {
        isItem = NO;
        feeds = [[NSMutableArray alloc] init];
    }
    
    if ([elementName isEqualToString:@"item"]) {
        isItem = YES;
        item = [[NSMutableDictionary alloc] init];
    }
    
    if ([elementName isEqualToString:@"title"]) {
        title = [[NSMutableString alloc] init];
    }
    
    if ([elementName isEqualToString:@"link"]) {
        link = [[NSMutableString alloc] init];
    }
    
    if ([elementName isEqualToString:@"pubDate"]) {
        pubDate = [[NSMutableString alloc] init];
    }
    
    if ([elementName isEqualToString:@"description"]) {
        desc = [[NSMutableString alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string;
{
    if ([element isEqualToString:@"title"]) {
        [title appendString:string];
    } else if ([element isEqualToString:@"link"]) {
        [link appendString:string];
    } else if ([element isEqualToString:@"description"]) {
        [desc appendString:string];
    } else if ([element isEqualToString:@"pubDate"]) {
        [pubDate appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName;
{
    if ([elementName isEqualToString:@"item"]) {
        
        if (title) [item setObject:title forKey:@"title"];
        if (link) [item setObject:[link stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"link"];
        if (desc) [item setObject:desc forKey:@"desc"];
        if (pubDate) [item setObject:pubDate forKey:@"pubDate"];

        [feeds addObject:[item copy]];
            
//        NSLog(@"Title:%@\r\nlink:%@\r\ndescription:%@\r\npubDate:%@\n\n", title, link, desc,  pubDate);
        
        isItem = NO;
        link = NULL;
        title = NULL;
        desc = NULL;
    } else {
        if (!isItem) {
//            if(link)self.rssLink = [link stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if(title)self.rssTitle = title;
            if(desc)self.rssDesc = desc;
        }
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_" object:nil];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    if (feeds) [self updateCoreData:feeds];
}

/*
 *  Saved parsed info to Core Data
 *
 */
- (void) updateCoreData:(NSArray *)feed
{
    NSManagedObjectContext *context = [RSSXMLParser managedObjectContext];
    
    // Find channel if exists
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"RSSChannel"];
    request.predicate = [NSPredicate predicateWithFormat:@"link == %@", rssLink];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES]];
    NSArray *results = [context executeFetchRequest:request error:nil];
    
    // Check if already exists
    if ([results count]) {
        
        RSSChannel *chan = [results objectAtIndex:0];
//        chan.date = [NSDate date];
//        chan.link = rssLink;
        chan.title = rssTitle;
        chan.desc = rssDesc;
        
        for (int i = 0; i < [self.feeds count]; i++) {
            
            NSEnumerator *enumObj = [chan.items objectEnumerator];
            NSDictionary *chanItem = [self.feeds objectAtIndex:i];
            
            
            NSString *fLink = [chanItem objectForKey:@"link"];
            NSString *fTitle = [chanItem objectForKey:@"title"];
            NSDate *fpubDate = [self getDateFormat:[chanItem objectForKey:@"pubDate"]];
        
            BOOL foundIN = NO;
            for(RSSItem *aKey in enumObj) {
                if (([aKey.pubDate isEqualToDate:fpubDate] && [aKey.link isEqualToString:fLink]) ||
                    ([aKey.title isEqualToString:fTitle] && [aKey.link isEqualToString:fLink])) {
                    foundIN = YES;
                    break;
                }
            }
            
            // If not found add new object to core data
            if (!foundIN) {
                RSSItem *newRSSItem = [NSEntityDescription insertNewObjectForEntityForName:@"RSSItem" inManagedObjectContext:context];
                newRSSItem.desc = [chanItem objectForKey:@"desc"];
                newRSSItem.pubDate = [self getDateFormat:[chanItem objectForKey:@"pubDate"]];
                newRSSItem.link = [chanItem objectForKey:@"link"];
                newRSSItem.title = [chanItem objectForKey:@"title"];
                newRSSItem.chan = chan;
                newRSSItem.read = NO;
                            
                [chan addItemsObject:newRSSItem];
            }
        }
        
    } else {
        // Create a new managed object
        RSSChannel *newRSSChannel = [NSEntityDescription insertNewObjectForEntityForName:@"RSSChannel" inManagedObjectContext:context];
        newRSSChannel.date = [NSDate date];
        newRSSChannel.link = self.rssLink ;
        newRSSChannel.title = self.rssTitle;
        newRSSChannel.desc = self.rssDesc;
        
        for (int i = 0; i < [self.feeds count]; i++) {
            NSDictionary *chanItem = [self.feeds objectAtIndex:i];
            
            RSSItem *newRSSItem = [NSEntityDescription insertNewObjectForEntityForName:@"RSSItem" inManagedObjectContext:context];
            newRSSItem.desc = [chanItem objectForKey:@"desc"];
            newRSSItem.pubDate = [self getDateFormat:[chanItem objectForKey:@"pubDate"]];
            newRSSItem.link = [chanItem objectForKey:@"link"];
            newRSSItem.title = [chanItem objectForKey:@"title"];
            newRSSItem.chan = newRSSChannel;
            newRSSItem.read = NO;
            
            [newRSSChannel addItemsObject:newRSSItem];
        }
        
        
    }
    
    NSError *error = nil;
    
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    
}


+ (void)removeByLink:(NSString *)str
{
    NSManagedObjectContext *context = [RSSXMLParser managedObjectContext];
    
    // Find channel if exists
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"RSSChannel"];
    request.predicate = [NSPredicate predicateWithFormat:@"link == %@", str];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES]];
    NSArray *results = [context executeFetchRequest:request error:nil];
    
    for (int i = 0; i < [results count]; i++) {
        RSSChannel *chan = [results objectAtIndex:i];
        
        [context deleteObject:chan];
        NSError *error = nil;
        
        // Save the object to persistent store
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }
    }
}

- (NSDate *)getDateFormat:(NSString *)str
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSString *myStr = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss z"];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    NSDate *date = [dateFormatter dateFromString:myStr];
    return date;
}

+ (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
