//
//  RSSChannel+CoreDataProperties.m
//  SimpleRSSFeeder
//
//  Created by Konstantin Beltikov on 21.12.15.
//  Copyright © 2015 none. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RSSChannel+CoreDataProperties.h"

@implementation RSSChannel (CoreDataProperties)

@dynamic date;
@dynamic desc;
@dynamic link;
@dynamic title;
@dynamic items;

@end
