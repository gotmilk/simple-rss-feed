//
//  RSSItem+CoreDataProperties.m
//  SimpleRSSFeeder
//
//  Created by Konstantin Beltikov on 21.12.15.
//  Copyright © 2015 none. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RSSItem+CoreDataProperties.h"

@implementation RSSItem (CoreDataProperties)

@dynamic desc;
@dynamic link;
@dynamic pubDate;
@dynamic read;
@dynamic title;
@dynamic chan;

@end
