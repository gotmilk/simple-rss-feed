//
//  RSSItem.h
//  SimpleRSSFeeder
//
//  Created by Konstantin Beltikov on 21.12.15.
//  Copyright © 2015 none. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RSSChannel;

NS_ASSUME_NONNULL_BEGIN

@interface RSSItem : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "RSSItem+CoreDataProperties.h"
