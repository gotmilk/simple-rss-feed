//
//  RSSItemsTableViewController.h
//  SimpleRSSFeeder
//
//  Created by Konstantin Beltikov on 19.12.15.
//  Copyright © 2015 none. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSSChannel.h"
#import "RSSItem.h"
#import "RSSXMLParser.h"
#import "WebViewController.h"

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
@interface RSSItemsTableViewController : UITableViewController<NSFetchedResultsControllerDelegate> {
    u_long newFetchLimit;
    u_long fetchOffset;
    u_long maxFetchCount;
}


@property (nonatomic, strong) NSString *channelURL;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@end
