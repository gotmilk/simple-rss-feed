//
//  WebViewController.h
//  SimpleRSSFeeder
//
//  Created by Konstantin Beltikov on 22.12.15.
//  Copyright © 2015 none. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "RSSXMLParser.h"

@interface WebViewController : UIViewController<UIWebViewDelegate>
{
    __weak UIWebView *_webView;
}

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString *itemURL;
@end
