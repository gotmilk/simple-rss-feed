//
//  RSSXMLParser.h
//  SimpleRSSFeeder
//
//  Created by Konstantin Beltikov on 19.12.15.
//  Copyright © 2015 none. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "RSSChannel.h"
#import "RSSItem.h"

@interface RSSXMLParser : NSObject<NSXMLParserDelegate>
{
    NSXMLParser *parser;
    BOOL isItem;
}

@property (nonatomic, strong) NSMutableArray *feeds;

@property (nonatomic, strong) NSMutableDictionary *item;
@property (nonatomic, strong) NSMutableString *title;
@property (nonatomic, strong) NSMutableString *link;
@property (nonatomic, strong) NSMutableString *pubDate;
@property (nonatomic, strong) NSMutableString *desc;

@property (nonatomic, strong) NSString *element;

@property (nonatomic, strong) NSString *rssLink;
@property (nonatomic, strong) NSString *rssTitle;
@property (nonatomic, strong) NSString *rssDesc;

// Delegate methods
- (instancetype)initWithContentsOfURL:(NSString *)url;
- (instancetype)initWithData:(NSData *)data;
- (instancetype)initWithStream:(NSInputStream *)stream NS_AVAILABLE(10_7, 5_0);

- (void)parse;


//
+ (void)removeByLink:(NSString *)str;
+ (NSManagedObjectContext *)managedObjectContext;

@end
