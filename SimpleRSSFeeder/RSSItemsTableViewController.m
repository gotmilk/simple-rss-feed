//
//  RSSItemsTableViewController.m
//  SimpleRSSFeeder
//
//  Created by Konstantin Beltikov on 19.12.15.
//  Copyright © 2015 none. All rights reserved.
//

#import "RSSItemsTableViewController.h"

@implementation RSSItemsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // Initialize Fetch Request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"RSSItem"];
    
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"chan.link == %@", self.channelURL];
//    fetchRequest.fetchLimit = 15;
    
    // Add Sort Descriptors
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"pubDate" ascending:NO]]];
    
    // Initialize Fetched Results Controller
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[RSSXMLParser managedObjectContext] sectionNameKeyPath:nil cacheName:@"RSS_ITEMS_CACHE"];
    
    
    // Configure Fetched Results Controller
    [self.fetchedResultsController setDelegate:self];
    
    [NSFetchedResultsController deleteCacheWithName:@"RSS_ITEMS_CACHE"];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
    
    if (error) {
        NSLog(@"Unable to perform fetch.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    /*
    maxFetchCount = [[[self.fetchedResultsController sections] objectAtIndex:0] numberOfObjects];
        
    newFetchLimit = 15;
    fetchOffset = 15;
    
    if (maxFetchCount < newFetchLimit) newFetchLimit = maxFetchCount;
    
    [self.fetchedResultsController.fetchRequest setFetchLimit:newFetchLimit];
    [NSFetchedResultsController deleteCacheWithName:@"RSS_ITEMS_CACHE"];
    */
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
    UIBarButtonItem *btnMark = [[UIBarButtonItem alloc] initWithTitle:@"Mark All" style:UIBarButtonItemStylePlain target:self action:@selector(mark:)];
    
    self.navigationItem.rightBarButtonItem = btnMark;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
//    self.navigationController.navigationItem.title=@"s";
}
/*
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger lastSectionIndex = [tableView numberOfSections] - 1;
    NSInteger lastRowIndex = [tableView numberOfRowsInSection:lastSectionIndex] - 1;
    if ((indexPath.section == lastSectionIndex) && (indexPath.row == lastRowIndex)) {
        // This is the last cell
        [self loadMore];
    }
}


// Load more
- (void)loadMore
{
    if (newFetchLimit < maxFetchCount) {
        newFetchLimit += fetchOffset;
        
        if (newFetchLimit > maxFetchCount) {
            newFetchLimit = maxFetchCount;
        }
        
        [self.fetchedResultsController.fetchRequest setFetchLimit:newFetchLimit];
        [NSFetchedResultsController deleteCacheWithName:@"RSS_ITEMS_CACHE"];
        NSError *error;
        if (![self.fetchedResultsController performFetch:&error]) {
            // Update to handle the error appropriately.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    
        [self.tableView reloadData];
    }
}
 */


-(IBAction)mark:(id)sender
{
    NSManagedObjectContext *context = [RSSXMLParser managedObjectContext];
     
    NSArray *sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:0];
    
    dispatch_async(kBgQueue, ^{
    for (int i = 0; i < [sectionInfo numberOfObjects]; i++) {
//         dispatch_sync(dispatch_get_main_queue(), ^{
        
        RSSItem *record = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        record.read = YES;
//         });
    }
        
        NSError *error = nil;
        
        // Save the object to persistent store
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }
    });
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    // Do your job, when done:
    RSSXMLParser *rssXMLParser = [[RSSXMLParser alloc] initWithContentsOfURL:self.channelURL];
    [rssXMLParser parse];
    [refreshControl endRefreshing];
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
    [self.tableView reloadData];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    switch (type) {
        case NSFetchedResultsChangeInsert: {
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeDelete: {
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeUpdate: {
            //            [self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        }
        case NSFetchedResultsChangeMove: {
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    NSArray *sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
 
//    return newFetchLimit;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RssItemCell" forIndexPath:indexPath];
    
    // Configure Table View Cell
    // Fetch Record
    RSSItem *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.detailTextLabel.textColor = [UIColor colorWithRed:0.5f green:0.5f blue:0.5f alpha:1.f];
    if (!record.read) {
        cell.textLabel.textColor = [UIColor colorWithRed:0.1f green:0.1f blue:0.1f alpha:1.f];
    } else {
        cell.textLabel.textColor = [UIColor colorWithRed:0.5f green:0.5f blue:0.5f alpha:1.f];
    }
    cell.textLabel.text = record.title;
    cell.detailTextLabel.text = record.link;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RssItemCell" forIndexPath:indexPath];
    
    // Configure Table View Cell
    // Fetch Record
    RSSItem *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    record.read = YES;
    
    NSManagedObjectContext *context = [RSSXMLParser managedObjectContext];
    NSError *error = nil;
        
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }

}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"DetailWebView"]) {
        
        WebViewController *itemsViewController = (WebViewController *)[segue destinationViewController];
        
        NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:selectedIndexPath];
        itemsViewController.itemURL = cell.detailTextLabel.text;
    }
}


@end
